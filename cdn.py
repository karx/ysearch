from flask import Blueprint, send_file

cdn = Blueprint("cdn", __name__)

# Shorthands for file locations


@cdn.route("/favicon.ico")
def favicon():
    return send_file("static/favicon.ico")


@cdn.route("/script.js")
def script():
    return send_file("static/ysearch.min.js")


@cdn.route("/refresh.js")
def refresh():
    return send_file("static/refresh.js")


@cdn.route("/style.css")
def style():
    return send_file("static/style.css")


@cdn.route("/error.css")
def error_css():
    return send_file("static/error.css")


@cdn.route("/toggle.js")
def toggle():
    return send_file("static/toggle.js")
