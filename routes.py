from flask import Blueprint, render_template
import util
from util import Counter

routes = Blueprint("routes", __name__)

# Renders html pages


@routes.route("/")
def home():
    return render_template("index.html")


@routes.route("/changelog")
def changelog():
    return render_template("changelog.html")


@routes.route("/info")
def info():
    if n := util.get_id_long():
        print(n)
        link_url = f"https://gitlab.com/karx/ysearch/commit/{n}"
    else:
        link_url = "https://gitlab.com/karx/ysearch/commits/master"
    c = Counter()
    return render_template(
        "info.html",
        commit=util.get_id_short(),
        commit_link=link_url,
        line_count=c.linecount,
        file_count=c.filecount,
    )
