from config import customFlask
from cli import get_debug, get_port
import os

app = customFlask("ySearch")

if __name__ == "__main__":
    app.run("0.0.0.0", debug=get_debug(), port=get_port(), use_reloader=True)
