import main


def test_app():
    app = main.app
    x = []
    with app.test_client() as c:
        x.append(c.get("/"))
        x.append(c.get("/changelog"))
        x.append(c.get("/info"))
        for response in x:
            assert response.status_code == 200
