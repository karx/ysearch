import ezflags

parser = ezflags.FlagParserExtended()
parser.add_flag('-n', '--no-debug', value=True, help="Turn debug mode off")
parser.add_argument('-p', '--port', type=int, help="Which port to use")

flags = parser.parse_args()


def get_debug():
    return not flags.no_debug


def get_port():
    return flags.port
