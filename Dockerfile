FROM python:3.8.3-buster
COPY . /data
WORKDIR /data
RUN pip install -r requirements.txt
EXPOSE 5000
CMD python ./main.py --no-debug -p 5000
