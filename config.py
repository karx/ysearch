from flask import Flask, render_template
from cdn import cdn
from routes import routes

# Declares custom Flask subclass


class customFlask(Flask):
    def __init__(self, import_name):
        super(customFlask, self).__init__(import_name)
        # Register
        self.register_error_handler(404, self.error_not_found)
        self.register_error_handler(500, self.error_internal)
        self.register_blueprint(cdn)
        self.register_blueprint(routes)
        # Remove jinja cache limit
        self.jinja_env.cache = {}

    def error_internal(self, error_message):
        return (
            render_template(
                "error.html",
                title="500 Internal Server Error",
                error_message="500 - Something went wrong on our end",
            ),
            500,
        )

    def error_not_found(self, error_message):
        return (
            render_template(
                "error.html",
                title="404 Not Found",
                error_message="404 - Page not found",
            ),
            404,
        )
