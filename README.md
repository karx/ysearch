# ySearch

[![Codeship Status for nerdstep710/ysearch](https://app.codeship.com/projects/2fa6d1d0-c864-0137-2edd-661ee1438eca/status?branch=master)](https://app.codeship.com/projects/367570)

ySearch is a search engine made with Python, HTML, CSS, and JavaScript.

It uses [Flask](https://www.palletsprojects.com/p/flask/), [Pure CSS](https://purecss.io/), and [jQuery](https://jquery.com/).

##  Contributing

Contributing to this project is easy! Get started [here](https://gitlab.com/nerdstep710/ysearch/blob/master/CONTRIBUTING.md).

## Known bugs

Please feel free to add you own bugs and any workarounds (if known) to this list.

- Loading animation never fades out. SOLVED: Fixed in v4.2.1 - Caused by a JavaScript bug.
