import glob
import os

import requests


# Various utility functions and classes


def get_repo():
    """
    Uses requests to send a request to the GitLab API.
    This method gets the latest commit from a repository.
    """
    repo_plain = requests.get(
        "https://gitlab.com/api/v4/projects/karx%2Fysearch/repository/commits/master",
        headers={"PRIVATE-TOKEN": "WCsse8ZDXWWQbzVnL_6N"},
    )
    repo_json = repo_plain.json()
    print(repo_json)
    return repo_json


def get_id_long():
    """Gets the long id of the latest commit"""
    repo_json = get_repo()
    try:
        id_long = repo_json["id"]
    except KeyError:
        id_long = None
    finally:
        print(id_long)
        return id_long


def get_id_short():
    """Same as get_id_long(), but gets the short id. Also, returns 'not found' instead of None because this function
    is exposed directly to the UI """
    repo_json = get_repo()
    try:
        id_short = repo_json["short_id"]
    except KeyError:
        id_short = "not found"
    finally:
        print(id_short)
        return id_short


def getlinecount(directory):
    """Count lines in file"""
    print(os.getcwd())
    if "templates" in os.getcwd():
        os.chdir("..")
    elif "static" in os.getcwd():
        os.chdir("..")
    os.chdir(directory)
    names = []
    for file_name in glob.glob("*.py"):
        with open(file_name) as f:
            names.append(
                sum(1 for line in f if line.strip() and not line.startswith("#"))
            )

    for file_name in glob.glob("*.html"):
        with open(file_name) as f:
            names.append(
                sum(1 for line in f if line.strip() and not line.startswith("<!--"))
            )

    for file_name in glob.glob("*.css"):
        with open(file_name) as f:
            names.append(
                sum(1 for line in f if line.strip() and not line.startswith("/*"))
            )

    for file_name in glob.glob("*.js"):
        with open(file_name) as f:
            names.append(
                sum(1 for line in f if line.strip() and not line.startswith("/*"))
            )
            names.append(
                sum(1 for line in f if line.strip() and not line.startswith("//"))
            )

    return sum(names)


class Counter:
    """Count things"""

    @property
    def linecount(self):
        """Implement getlinecount()"""
        x = [
            getlinecount("."),
            getlinecount("./templates"),
            getlinecount("./static"),
        ]

        return sum(x)

    @property
    def filecount(self):
        """Count files"""
        print(os.getcwd())
        if "templates" in os.getcwd():
            os.chdir("..")
        elif "static" in os.getcwd():
            os.chdir("..")
        x = [
            len([i for i in os.listdir(".") if not i.startswith(".")]),
            len([i for i in os.listdir(".") if not i.startswith("__")]),
            len([i for i in os.listdir("templates") if not i.startswith(".")]),
            len([i for i in os.listdir("static") if not i.startswith(".")]),
        ]
        return sum(x)
